<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/custom.css', array( $parent_style ));
}

/* Add featured image to feed in a <media:content /> tag */
// Add namespace for media:image element used below
add_filter( 'rss2_ns', function(){
  echo 'xmlns:media="http://search.yahoo.com/mrss/"';
});
// insert the image object into the RSS item (see MB-191)
add_action('rss2_item', function(){
  global $post;
  if (has_post_thumbnail($post->ID)){
    $thumbnail_ID = get_post_thumbnail_id($post->ID);
    $thumbnail = wp_get_attachment_image_src($thumbnail_ID, 'large');
    if (is_array($thumbnail)) {
      echo '<media:content medium="image" url="' . $thumbnail[0]
        . '" width="' . $thumbnail[1] . '" height="' . $thumbnail[2] . '" />';
    }
  }
});